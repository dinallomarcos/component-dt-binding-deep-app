import { Component, OnInit, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {
  @Output() serverCreated = new EventEmitter<{ name: string, content: string }>();
  // tslint:disable-next-line:no-output-rename
  @Output('bpCreated') bluePrintCreated = new EventEmitter<{ name: string, content: string }>();
  @ViewChild('contentInput', { static: true }) contentInput: ElementRef;
  newServerName = '';
  newServerContent = '';
  constructor() { }

  ngOnInit() {
  }

  onAddServer(serverInput: HTMLInputElement) {
    this.serverCreated.emit({ name: serverInput.value, content: this.contentInput.nativeElement.value });
    this.clearServerFilds();
  }

  onAddBlueprint(serverInput: HTMLInputElement) {
    this.bluePrintCreated.emit({ name: serverInput.value, content: this.contentInput.nativeElement.value });
    this.clearServerFilds();
  }

  private clearServerFilds() {
    this.newServerName = '';
    this.newServerContent = '';
  }

}
