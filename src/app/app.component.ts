import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'component-dt-binding-deep-app';
  serverElements = [{ type: 'server', name: 'TestServer', content: 'Just a test!' }];

  onServerCreated(serverdata: { name: string, content: string }) {
    this.serverElements.push({
      type: 'server',
      name: serverdata.name,
      content: serverdata.content
    });
  }

  onBluePrintCreated(bluePrintData: { name: string, content: string }) {
    this.serverElements.push({
      type: 'blueprint',
      name: bluePrintData.name,
      content: bluePrintData.content
    });
  }
}
